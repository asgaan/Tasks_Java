public class square {
	public static void main(String[] args) {
		int width = Integer.parseInt(args[0]);
		int height = Integer.parseInt(args[1]);


		// First width line
		for (int i = 0; i < width; i++) {
			System.out.print("#");
		}


		// Creates height amount of lines (-2 due to width top and bot)
		for (int j = 0; j < height-2; j++) {
			System.out.println();
			System.out.print("#");

			int spaces = width-2;
			for (int n = 0; n < spaces; n++) {
				System.out.print(" ");
			}
			System.out.print("#");
		}

		// Just shifting to next empty line
		System.out.println();


		// Bottom width line.
		for (int m = 0; m < width; m++) {
			System.out.print("#");
		}

	}
}