import java.io.File;
import java.awt.Desktop;
import java.io.IOException;

import java.util.ArrayList;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class application {


	public static void main(String[] args) throws IOException {
		File file = new File("small.txt");

		//Opens the file.
		Desktop desktop = Desktop.getDesktop();
		desktop.open(file);

		
		// Try-catch with scanner
		int count = 0;
		int countW = 0;
		String [] words = null; 
		try {;
			Scanner myReader = new Scanner(file);

			while (myReader.hasNextLine()) {
				count++;
				String data = myReader.nextLine();

				// Counting words from argument
				words = data.split("\\W+");
				for(String word : words) {
					if(word.toLowerCase().contains(args[0].toLowerCase())){
						countW++;
					}
				}
			}
			myReader.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		}



		// Making the bytes into KB.
		Double kb = ((double)file.length())/1024;
		String kb_deci = String.format("%.4f", kb);

		System.out.println("File name: " + file.getName());
		System.out.println("File size in kilobytes " + kb_deci);
		System.out.println("The file has this many lines: " + count);
		System.out.println("Your word was found this many times: " + countW);

	}
}