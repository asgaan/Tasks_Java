public class bmi {

	public static void main(String[] args) {
		
		// Defining weight and height from argument inputs
		int weight = Integer.parseInt(args[0]);
		float height = Float.parseFloat(args[1]);

		// Defining BMI, limiting it to two decimals.
		float BMI = weight/(height*height);
		String BMI_Float = String.format("%.2f", BMI);


		System.out.println("Your BMI is " + BMI_Float);


		// Checking what category the BMI is in.
		String category = "";

		if (BMI < 18.5) {
			category = "You're underweight";
		}
		else if (BMI >= 18.5 && BMI < 25) {
			category = "You have a normal weight";
		}
		else if (BMI >= 25 && BMI < 30) {
			category = "You're overweight";
		}
		else if (BMI >= 30) {
			category = "You're obese";
		}
		else {
			category = "Something went wrong with the calculation";
		}

		System.out.println(category);
	}
}