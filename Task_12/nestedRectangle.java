public class nestedRectangle {

	public static void space() {
		System.out.print(" ");
	}

	public static void main(String[] args) {
		int width = Integer.parseInt(args[0]);
		int height = Integer.parseInt(args[1]);


		// First width line
		for (int i = 0; i < width; i++) {
			System.out.print("#");
		}


		// Creates height amount of lines (-2 due to width top and bot)
		for (int j = 0; j < height-2; j++) {
			System.out.println();
			System.out.print("#");

			int spacesOne = width-2;
			int spacesTwo = width-6;
			int innerTop = width-4;


			// Checks its the first line after or before the top/bot.
			if (j==0 || j==(height-3) ) {
				for (int y = 0; y < spacesOne; y++) {
					space();
				}
			}

			// Checks if its the inner top/bot, fills it.
			else if (j==1 || j==(height-4)) {
				space();
				for (int z = 0; z < innerTop; z++) {
					System.out.print("#");
				}
				space();
			}

			// Add "walls" to the inner rectangle.
			else {
				space();
				System.out.print("#");
				for (int x = 0; x < spacesTwo; x++) {
					space();
				}
				System.out.print("#");
				space();
			}
			System.out.print("#");
		}

		// Just shifting to next empty line
		System.out.println();


		// Bottom width line.
		for (int m = 0; m < width; m++) {
			System.out.print("#");
		}

	}
}