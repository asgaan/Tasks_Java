import java.util.ArrayList;
import java.util.List;

public class nameSearch {

	public static void main(String[] args) {

		ArrayList<Person> people = new ArrayList<Person>();
		people.add(new Person("John", "Lennon"));
		people.add(new Person("Ringo", "Starr"));
		people.add(new Person("Paul", "McCartney"));
		people.add(new Person("George", "Harrison"));
		people.add(new Person("Yoko", "Ono"));

		for (Person person: people ) {
			if(person.getName().toLowerCase().contains(args[0].toLowerCase())) {
				System.out.println(person.getName());
			}
		}
	}
}
