public class Person {

	//Fields//
	private String fName;
	private String lName;
	private String phoneNo;

	//Constructors//
	public Person(String fName, String lName, String phoneNo) {
		this.fName = fName;
		this.lName = lName;
		this.phoneNo = phoneNo;
	}

	public Person(String fName) {
		this.fName = fName;
	}

	public Person(String fName, String lName) {
		this.fName = fName;
		this.lName = lName;
	}

	public String getName(){
		return this.fName+" "+lName;
	}

	public String getNumber(){
		return this.phoneNo;
	}
}