public class nameSearch {

	public static void main(String[] args) {
		
		String[] theBeatles = new String[]{
			"John Lennon",
			"Ringo Starr",
			"Paul McCartney",
			"George Harrison",
			"Yoko Ono"
		};

		
		for (String person : theBeatles) {
			if(person.contains(args[0])) {
				System.out.println(person);
			}
		}
	}
}